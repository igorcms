<?php

/**
 * 404 error page
 * 
 * this module gets loaded when the requested module could not be found 
 */ 
class m_e404
{
	public function main($args)
	{
		global $template;
		
		$template->display('e404_main.html');
	}
}