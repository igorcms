<?php

/**
 * index
 * 
 * the main page that gets loaded if no module is specified 
 */ 
class m_index
{
	public function main($args)
	{
		global $template;
		
		$template->assign(array(
			'TEST'		=> request_var('test', ''),
		));
		
		$template->display('index_main.html');
	}
}