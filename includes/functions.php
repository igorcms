<?php

function request_var($name, $default, $cookie = false)
{
	$value = null;
	
	if (isset($_REQUEST[$name]))
	{
		if ($cookie && isset($_COOKIE[$name]))
		{
			$value = $_COOKIE[$name];
		}
		else
		{
			$value = (isset($_POST[$name])) ? $_POST[$name] : $_GET[$name];
		}
		
		if (!is_null($value))
		{
			$type = gettype($default);
			
			if ($type === 'array')
			{
				// @todo
			}
			else if ($type === 'string')
			{
				$value = (string) $value;
				$value = format_html::encode($value);
			}
			else
			{
				settype($value, $type);
			}
		}
	}
	
	if (is_null($value))
	{
		$value = $default;
	}
	
	return $value;
}

function list_files($root, $dir = '', $exclude = array())
{
	$exclude = array_merge($exclude, array('.', '..'));
	
	$return = array();
	
	if ($dh = opendir($root . $dir))
	{
		while (false !== ($file = readdir($dh)))
		{
			if (in_array($file, $exclude))
			{
				continue;
			}
			
			if (is_file($root . $dir . $file))
			{
				$return[] = $dir . $file;
			}
			else if ($recurse)
			{
				$return = array_merge($return, list_files($root, $dir . $file . '/', $exclude));
			}
		}
		
		closedir($dh);
	}
	
	return $return;
}