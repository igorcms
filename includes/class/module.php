<?php

/**
 * a module class looks like this:
 * 
 * the module name is the class name,
 * the actions are methods,
 * the args get passed to the method  
 * 
 * class m_modulename
 * {
 * 	public function main($args)
 * 	{
 *  }
 *  public function some_action($args)
 *  {
 *  }
 *  public function other_action($args)
 *  {
 *  }     
 * }  
 */ 

class module
{
	private $module_path;
	private $module_prefix;
	private $module_default;
	
	public function __construct($module_path, $module_prefix = '', $module_default = 'e404')
	{
		$this->module_path = $module_path;
		$this->module_prefix = $module_prefix;
		$this->module_default = $module_default;
	}
	
	public function load($module_name, $action, $args = array())
	{
		if (!file_exists($this->module_path . $module_name . '.' . PHP_EXT))
		{
			$module_name = $this->module_default;
		}
		
		include($this->module_path . $module_name . '.' . PHP_EXT);
		
		$module_class = $this->module_prefix . $module_name;
		
		$module = new $module_class();
		
		if (!method_exists($module, $action))
		{
			$action = 'main';
		}
		
		call_user_func(array($module, $action), $args);
	}
}