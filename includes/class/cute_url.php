<?php

class cute_url
{
	private $base_url;
	private $url, $_url;
	
	public function __construct($url_rewritten = true)
	{
		$this->base_url = dirname($_SERVER['SCRIPT_NAME']);
		
		$url = substr($_SERVER['REQUEST_URI'], strlen($this->base_url));
		if ($url[0] === '/')
		{
			$url = substr($url, 1);
		}
		
		// url_rewritten = we use htaccess
		if (!$url_rewritten)
		{
			$url = substr(strchr($url, '/'), 1);
		}
		
		$this->url = $this->_url = array_diff(explode('/', $url), array(''));
		
		if (substr($this->base_url, 0, -1) !== '/')
		{
			$this->base_url .= '/';
		}
	}
	
	public function get($default, $html = false)
	{
		if (!sizeof($this->_url))
		{
			return $default;
		}
		
		$value = array_shift($this->_url);
		
		if (is_string($default) && $html)
		{
			return format_html::encode((string) $value);
		}
		
		settype($value, gettype($default));
		
		return $value;
	}
	
	public function get_remaining($html = false)
	{
		if (!sizeof($this->_url))
		{
			return array();
		}
		
		$value = $this->_url;
		
		$this->_url = array();
		
		if ($html)
		{
			return array_map(array('format_html', 'encode'), $value);
		}
		
		return $value;
	}
}