<?php

if (!defined('ROOT_PATH'))
{
	exit;
}

require(ROOT_PATH . 'includes/class/dbal.' . PHP_EXT);

class dbal_mysqli extends dbal
{
	public function sql_connect($dbhost, $dbport, $dbname, $dbuser, $dbpass)
	{
		if (!$this->db_conn = mysqli_connect($dbhost . (!empty($dbport) ? ":$dbport" : ''), $dbuser, $dbpass))
		{
			throw new ErrorException('dbal: failed to connect to database');
		}
		
		mysqli_query($this->db_conn, "SET NAMES 'utf8'");
		
		if (!mysqli_select_db($this->db_conn, $dbname))
		{
			throw new ErrorException('dbal: failed to select db');
		}
	}
	
	public function sql_query($sql)
	{
		$result = mysqli_query($this->conn_id, $sql);
		
		if (!$result)
		{
			throw new ErrorException("dbal: query failed\n\n" . mysqli_error($this->conn_id));
		}
		
		return $result;
	}
	
	public function sql_fetchrow($result)
	{
		return mysqli_fetch_assoc($this->conn_id, $result);
	}
	
	public function sql_freeresult($result)
	{
		return mysqli_free_result($result);
	}
	
	public function sql_close()
	{
		return mysqli_close($this->conn_id);
	}
}