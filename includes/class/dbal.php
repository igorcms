<?php

abstract class dbal
{
	protected $db_conn;
	
	abstract public function sql_connect($dbhost, $dbport, $dbname, $dbuser, $dbpass);
	abstract public function sql_query($sql);
	abstract public function sql_fetchrow($result);
	abstract public function sql_freeresult($result);
	abstract public function sql_close();
}