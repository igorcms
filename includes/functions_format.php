<?php

class format_html
{
	public static function encode($input)
	{
		return htmlspecialchars($input);
	}
	
	public static function decode($input)
	{
		return htmlspecialchars_decode($input);
	}
}

class format_javascript
{
	public static function encode($input)
	{
		return addslashes($input);
	}
	
	public static function decode($input)
	{
		return stripslashes($input);
	}
}