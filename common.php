<?php

if (!defined('ROOT_PATH'))
{
	exit;
}

error_reporting(E_ALL);

// includes
include(ROOT_PATH . 'includes/functions.' . PHP_EXT);
include(ROOT_PATH . 'includes/functions_format.' . PHP_EXT);
include(ROOT_PATH . 'includes/class/module.' . PHP_EXT);
include(ROOT_PATH . 'includes/class/cute_url.' . PHP_EXT);

// vendors
foreach (array_diff(scandir(ROOT_PATH . 'vendor/'), array('.', '..')) as $vendor)
{
	if (file_exists(ROOT_PATH . 'vendor/' . $vendor . '/vendor.' . PHP_EXT))
	{
		include(ROOT_PATH . 'vendor/' . $vendor . '/vendor.' . PHP_EXT);
	}
}

// general objects
$module = new module(ROOT_PATH . 'modules/', 'm_');
$cute_url = new cute_url(file_exists(ROOT_PATH . '.htaccess'));

// db
include(ROOT_PATH . 'includes/class/dbal_mysqli.' . PHP_EXT);
$db = new dbal_mysqli();
try
{
	$db->sql_connect('localhost', NULL, 'cms', 'root', NULL);
}
catch (Exception $e)
{
	echo '<h1>an error occured</h1>';
	echo $e->getMessage();
	exit;
}

// template
$template->assign('ROOT_PATH', $_SERVER['SCRIPT_NAME'] === '/' ? '/' : dirname($_SERVER['SCRIPT_NAME']) . '/');