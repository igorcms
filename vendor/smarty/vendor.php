<?php

// smarty 2.6.20

if (!defined('ROOT_PATH'))
{
	exit;
}

include(ROOT_PATH . 'vendor/smarty/Smarty.class.' . PHP_EXT);

$template = new Smarty();
$template->template_dir = ROOT_PATH . 'styles/default/template/';
$template->compile_dir = ROOT_PATH . 'cache/template/';
//$template->use_sub_dirs = true;